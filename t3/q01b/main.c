#include <stdio.h>

int main(){

	srand(time(NULL));
	int numero = gera_primo();
	
	if(testa_primo(numero)==1){
		printf("O numero gerado %d e primo!\n", numero);
	}
	else{
		printf("O numero gerado %d nao e primo!\n", numero);
	}

	return 0;
}