#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int main(){

	void *handle;
	int (*numero)();
	int (*teste_numero_primo)(int);
  	char *error;

	srand(time(NULL));

	handle = dlopen("./liblibprimo.so",RTLD_LAZY);
	if(!handle)
	{
		fprintf(stderr, "%s\n", dlerror());
		exit(1);
	}

	numero = dlsym(handle, "gera_primo");

	if((error = dlerror()) != NULL)
	{
		fprintf(stderr,"%s\n", error);
		exit(1);
	}

	teste_numero_primo = dlsym(handle, "testa_primo");

	if((error = dlerror()) != NULL)
	{
		fprintf(stderr,"%s\n", error);
		exit(1);
	}

	printf("O numero gerado %d eh primo!\n", (*numero)());
	
	return 0;
}