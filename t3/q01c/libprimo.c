#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "libprimo.h"

int gera_primo(){
	int primo = rand() % 1000;
	if(testa_primo(primo)==1)
		return primo;
	else
		gera_primo();
}

int testa_primo(int primo){
	int c;
	for ( c = 2 ; c <= primo - 1 ; c++ )
	{
		if ( primo%c == 0 )
		{
			return 0;
		}
	}
	if ( c == primo )
		return 1;
}