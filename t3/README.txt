Respostas para as questões do trabalho

Em relação ao programa que contempla os itens a) e b), quais foram as alterações
de códigos-fonte necessárias para a solução (se houverem)?

A única alteração feita no código-fonte foi a remoção da inclusão da biblioteca, pois a mesma foi feita na compilação do programa.

Dados os conhecimentos adquiridos em função desse trabalho, indique vantagens
e problemas decorrentes da utilização de bibliotecas dinâmicas.
