Este é o código do trabalho 3, letra a de Fundamentos de Sistemas Operacionais.
Desenvolvido no sistema operacional Debian 8, no compilador gcc versão 4.9.2. 
Criação de bibliotecas estáticas.

Compilação:

	Para compilar o programa é necessário a execução do comando:
		$ make
	e agora existirá um binário com o nome de `prog`

Execução:

	Para executar o programa, é necessário a execução do comando:
		$ ./prog
	O programa irá gerar um número primo e irá verificar se o número gerado é primo ou não.