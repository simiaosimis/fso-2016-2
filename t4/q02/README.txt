Programa para demonstrar busca de arquivos e manipulação de diretórios.
Desenvolvido no sistema operacional Debian 8, no compilador gcc versão 4.9.2. 
Código desenvolvido no editor de texto Vim.

Compilação:

	Para compilar o programa é necessário a execução do comando:
		$ make
	e agora existirá um binário com o nome de `prog`

Execução
	
	Para executar o programa, é necessário a execução do comando:
		$ ./prog dir query n
	Onde `dir` é o diretório raiz onde a busca será executada, `query` é 
	o texto a ser buscado no nome dos arquivos e `n` é quantos resultados
	serão apresentados.

Programa

	O programa utiliza da biblioteca dirent.h para manipular os diretórios,
	a verificação da presença da substring no nome do arquivo é feita com 
	uma checagem linear (O(tamanho da query * tamanho do nome do arquivo))
	simples, a travessia dos diretórios é feita numa recursividade que
	processa o valor atual e chama a própria função para processar os
	subdiretórios.

Questão discursiva: descreva o que é Filesystem Hierarchy Standard (FHS) e indique qual é a destinação típica das pastas tipicamente encontradas na raiz de um sistema:

	O Filesystem Hierarchy Standard define os principais diretórios, e o seu conteúdo, em um sistema operacional Linux

	/bin/ : Comandos binários essenciais para todos os usuários (ex: cat, ls, cp)
	/boot/ : Arquivos do Boot loader (ex: núcleo, initrd).
	/dev/ : Dispositivos (ex: /dev/null).
	/etc/ : Arquivos de configuração específicos do computador.
	/home/ : Diretórios de usuários.(Opcional)
	/lib/ : Diretório com as bibliotecas essenciais para os arquivos binários contidos nos diretórios /bin/ e /sbin/.
	/mnt/ : Sistemas de arquivos "montados" temporariamente.
	/media/ : Pontos de "montagem" para mídia removível, como CD-ROMs 
	/opt/ : Pacotes estáticos de aplicações.
	/proc/ : Sistemas de arquivo virtual, que possui o estado do núcleo e processos do sistema; a maioria dos arquivos é baseada no formato texto (ex: tempo de execução, rede).
	/root/ : Diretório home para o super usuário (root).(Opcional)
	/sbin/ : Arquivos binários para propósito de administração do sistema.
	/tmp/ : Arquivos temporários. (Ver também /var/tmp).
	/srv/ : Dados específicos que são servidos pelo sistema.
	/usr/ : Hierarquia secundária para dados compartilhados de usuários, cujo acesso é restrito apenas para leitura.
	/var/ : Arquivos "variáveis", como logs, base de dados, páginas Web e arquivos de e-mail.
