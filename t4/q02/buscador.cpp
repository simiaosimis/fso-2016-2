#include<stdio.h>
#include<iostream>
#include<dirent.h>
#include<string>
using namespace std;
string query;
int n = 1;
int limite;

void print_file(string dir){
	FILE *file = fopen(dir.c_str(), "r");
	if(file == NULL) return;
	string s;
	for(int i=0; i<30; ++i){
		char c = fgetc(file);
		if(c != EOF) s += c;
		else break;
	}
	cout << s << endl;
	fclose(file);
}

bool substring(string s){
	for(int i=0; i<=(int)s.size()-(int)query.size(); ++i){
		for(int j=0; j<(int)query.size(); ++j){
			if(query[j] != s[i+j]){
				break;
			}
			if(j == ((int)query.size()-1)) return true;
		}
	}
	return false;
}

void recursivo(string base, string name){
//	cout << "base: " << base << endl;
//	cout << "name: "<< name << endl;
	if(name == "." || name == "..") return;
	string cur = base + (base.empty() ? "" : base[base.size()-1] == '/' ? "" : "/") + name;
	DIR *dir;
	if((dir= opendir(cur.c_str())) != NULL){
		struct dirent *ent;
		while((ent = readdir(dir)) != NULL){
			string file_name = ent->d_name;
			if(ent->d_type == DT_REG){
				if(substring(file_name)){
					if(n > limite) break;
					string s = cur + (cur[cur.size()-1] == '/' ? "" : "/") + file_name;
					cout << n << " " << s << " --" << endl;
					print_file(s);
					n++;
				}
			}
			recursivo(cur, file_name);
		}
		closedir(dir);
	}
	else{
//		cout << "ELSE" << endl;
		return;
	}
//	cout << "saindo" << endl; 
}

int main(int argc, char* argv[]){
	if(argc < 2) return 0;
	query = argv[2];
	limite = stoi(argv[3]);
	recursivo("", argv[1]);
	return 0;
}
