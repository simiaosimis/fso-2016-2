#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <utime.h>
#include <sys/types.h>
#include <string.h>

void change_timestamps(char const *filename, char const *date){
	int i = 0;
	char year[4], month[3], day[3], hour[3], minutes[3];
	
	//handling year
	for(i = 0; i < 4; i++){
		year[i] = date[i];
	}
	year[4] = '\0';

	//handling months
	for(i = 4; i < 6; i++){
		month[i - 4] = date[i];
	}
	month[2] = '\0';

	//handling days
	for(i = 6; i < 8; i++){
		day[i - 6] = date[i];
	}
	day[2] = '\0';

	//handling hours
	for(i = 8; i < 10; i++){
		hour[i - 8] = date[i];
	}
	hour[2] = '\0';

	//handling minutes
	for(i = 10; i < 12; i++){
		minutes[i - 10] = date[i];
	}
	minutes[2] = '\0';

	struct tm date_structure;

	date_structure.tm_year = (atoi(year))-1900;
	date_structure.tm_mon = (atoi(month))-1;
	date_structure.tm_mday = (atoi(day));
	date_structure.tm_hour = (atoi(hour));
	date_structure.tm_min = (atoi(minutes));
	date_structure.tm_sec = 0;
	date_structure.tm_isdst = 1;

	time_t new_time = mktime(&date_structure);

	struct utimbuf time_buffer;
	time_buffer.actime = new_time;
	time_buffer.modtime = new_time;

	utime(filename, &time_buffer);
}


void create_backup(char *file_name)
{
        char *backup_name=malloc(sizeof(file_name));
        strcpy(backup_name, file_name);

        int len=strlen(file_name);

        backup_name[len-3]='b';
        backup_name[len-2]='k';
        backup_name[len-1]='p';
 	
        char command[256];
 		strcpy(command, "cp ");
        strcat(command, file_name);
        strcat(command, " ");
        strcat(command, backup_name);
        system(command);

}

int main(int argc, char *argv[])
{
	if(argc != 3){
		printf("Invalid number of parameters\n");
		printf("Usage: ./prog (File Name) (Date. Format: YYYYMMDDHHmm)\n");
		return 0;
	}
    char *file_name = argv[1];
    char *date = argv[2];

    if(strlen(date) != 12){
    	printf("Invalid format for date\n");
    	printf("Correct format: YYYYMMDDHHmm\n");
    	return 0;
    }

    FILE *file = fopen(file_name, "r");
    if (!file) {
        printf("Invalid file.\n");
        return 0;
    } 

    create_backup(file_name);

    fclose(file);

    struct stat fs;
    stat(file_name, &fs);
    printf("Creation time: %s\n", ctime(&fs.st_ctime));
    printf("Acess time: %s\n", ctime(&fs.st_atime));
    printf("Modification time: %s\n", ctime(&fs.st_mtime));

    change_timestamps(file_name, date);

    return 0;
}