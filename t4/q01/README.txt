Programa para demonstrar busca de arquivos e manipulação de diretórios.
Desenvolvido no sistema operacional Debian 8, no compilador gcc versão 4.9.2. 
Código desenvolvido no editor de texto Vim.

Compilação:

	Para compilar o programa é necessário a execução do comando:
		$ make
	e agora existirá um binário com o nome de 'muda_horario'


Execução
	
	Para executar o programa, é necessário a execução do comando:
		$ ./muda_xhorario file date
	Onde 'file' é o arquivo que será alterado e 'date' é a data que será colocada nas
	informações do arquivo. O formato da data deve ser AAAAMMDDHHmm onde AAAA são os 
	dígitos relativos ao ano, MM são os dígitos relativos ao mês, DD são os dígitos 
	relativos aos dia, HH são os dígitos relativos à hora e mm são os dígitos 
	relativos aos minutos

Limitações Conhecidas
	
	O programa não consegue alterar a data de criação do arquivo.

Programa

	O programa utiliza a função utime da time.h para realizar a alteração das datas de 
	modificação e acesso do arquivo selecionado, printando na tela os valores 
	originais do arquivo. Além disso, o programa cria um backup (extensão .bkp) do 
	arquivo de entrada.