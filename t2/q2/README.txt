Programa para demonstrar sinalização entre processos.
Desenvolvido no sistema operacional Debian 8, no compilador gcc versão 4.9.2. 
Código desenvolvido no editor de texto Vim.

Compilação:

	Para compilar o programa é necessário a execução do comando:
		- Versão Concorrente
		$ make concorrente
		- Versão Sequencial
		$ make sequencial
	e agora existirá um binário com o nome de `prog`

Execução:
	
	Para executar o programa, é necessário a execução do comando:
		$ ./prog <quantidade_de_numero> n[0] n[1] n[2] ... n[n-1]
	onde o nome do programa é seguido da quantidade de numeros a
	serem analisados, devendo ser menor que 100, seguido dos números.
	Em caso de a quantidade de numeros informada seja maior que os
	numeros, o programa retornará segmentation fault, pois tentará
	ler números que não existem.

Limitações:

	Na versão concorrente são criadas N*(N-1)/2 (formação de todos os 
	pares únicos de i com j, excluindo pares de i com i e j com j, em 
	caso de todos os pares possíveis seria N^2) threads o que leva a
	complexidade O(N^2);
	Por conta da complexidade O(N^2) a versão concorrente se mostra
	menos eficiente que a solução sequencial (O(N)) nos benchmarkings
	realizados, o que acontece é que a solução concorrente perde em
	desempenho já na criação das threads.
	A solução sequencial já é o algoritmo lower bound para valores não
	ordenados, ou seja, não é possível ser otimizado.

