#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdint.h>
#define MAX 100
int w[MAX], v[MAX];
void *init(void *arg){
	int cur = (int)arg;	
	w[cur] = 1;
}
void *comp(void *arg){
	int i = ((int)arg)/MAX;
	int j = ((int)arg)%100;
	printf("Thread T(%d,%d) compares x[%d] = %d and x[%d] = %d, and writes 0 into ", i, j, i, v[i], j, v[j]);
	if(v[i] < v[j]){
		w[i] = 0;
		printf("w[%d]\n", i);
	}
	else{
		w[j] = 0;
		printf("w[%d]\n", j);
	}
}
void *print(void *arg){
	int cur = (int)arg;
	if(w[cur]){
		printf("Maximum = %d\n", v[cur]);
		printf("Location = %d\n", cur);
	}
}

int main(int argc, char* argv[]){
	int i,j;
	int n;
	n=atoi(argv[1]);
	if(n > 100) n = 100;
	printf("Number of input values = %d\n", n);
	printf("Input values x =");
	for(i=2; i< n+2; i++){
		v[i-2] = atoi(argv[i]);
		printf(" %d", v[i-2]);
	}
	printf("\nAfter initialization w =");
	pthread_t t[MAX], t2[MAX][MAX], t3[MAX];
	for(i=0; i<n; ++i){
		int r = pthread_create(&t[i], NULL, init, (void*)(uintptr_t)i);
		if(r) printf("deu merda amigos\n");
	}
	for(i=0; i<n; ++i){
		pthread_join(t[i], NULL);
		printf(" %d", w[i]);
	}
	printf("\n");
	for(i=0;i<n;i++){
		for(j=i+1;j<n;j++){
			int r = pthread_create(&t2[i][j], NULL, comp, (void*)(uintptr_t)(i*100 + j));
			if(r) printf("deu merda amigos\n");
		}
	}
	for(i=0;i<n;i++){
		for(j=i+1;j<n;j++){
			pthread_join(t2[i][j], NULL);
		}
	}
	printf("After Step 2\nw =");
	for(i=0; i<n; ++i){
		printf(" %d", w[i]);
	}
	printf("\n");
	for(i=0; i<n; ++i){
		int r = pthread_create(&t3[i], NULL, print, (void*)(uintptr_t)i);
		if(r) printf("deu merda amigos\n");
	}
	for(i=0; i<n; ++i){
		pthread_join(t3[i], NULL);
	}
}
