#include <stdio.h>
#include <stdlib.h>
#define MAX 105

int main(int argc, char* argv[]){
	int n = atoi(argv[1]), i, answer = -(1<<30);
	for(i=0; i<n; ++i){
		int cur = atoi(argv[i+2]);
		if(cur > answer){
			answer = cur;
		}
	}
	printf("%d\n", answer);
}
