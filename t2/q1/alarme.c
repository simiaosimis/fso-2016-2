#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

void handler(int signal){
	if(signal == SIGALRM)
		printf("Received a signal. ID: %d\n", signal);
}

void spawn(){
	pid_t child_pid;

	child_pid = fork();

	if(child_pid == 0){
		sleep(5);
		kill(getppid(), SIGALRM);
	}
	else{
		pause();
	}
}

int main(int argc, char **argvs){
	signal(SIGALRM, handler);

	spawn();

	return 0;
}