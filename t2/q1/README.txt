Programa para demonstrar sinalização entre processos.
Desenvolvido no sistema operacional Debian 8, no compilador gcc versão 4.9.2. 
Código desenvolvido no editor de texto Vim.

Compilação:

	Para compilar o programa é necessário a execução do comando:
		$ make
	e agora existirá um binário com o nome de `prog`

Execução
	
	Para executar o programa, é necessário a execução do comando:
		$ ./prog
