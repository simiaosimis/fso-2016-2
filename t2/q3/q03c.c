#include<stdio.h>
#include<pthread.h>
#include <stdint.h>
#include<stdlib.h>
#define MAX 105
int n1,m1,n2,m2,mat1[MAX][MAX],mat2[MAX][MAX],resultado[MAX][MAX];
void *calculate(void *arg){
    int i = ((int)arg)/1000;
    int j = ((int)arg)%1000;
	int cur = 0;
	for(int k=0; k<n2; ++k){
		cur += mat1[i][k]*mat2[k][j];
	}
	resultado[i][j] = cur;
}

int main(){
	FILE *fp;
	char com[100];
	fp = popen("cat /proc/cpuinfo | grep -c processor", "r");
	if (fp == NULL) {
		printf("Failed to run command\n" );
		exit(1);
	}
	fgets(com, sizeof(com)-1, fp);
	int qtd = atoi(com);
	scanf("%d %d %d %d", &n1, &m1, &n2, &m2);
	if(m1!=n2){
		printf("Invalid matrix multiplication\n");
		return 0;
	}
	for(int i=0; i<n1; ++i){
		for(int j=0; j<m1; ++j){
			scanf("%d", &mat1[i][j]);
		}
	}
	for(int i=0; i<n2; ++i){
		for(int j=0; j<m2; ++j){
			scanf("%d", &mat2[i][j]);
		}
	}
	pthread_t t[qtd];
	for(int i=0; i<m2; ++i){
		for(int j=0; j<n1; ++j){
    		int r = pthread_create(&t[(i*m2 + j)%qtd], NULL, calculate, (void*)(uintptr_t)(i*1000 + j));
			if(r) printf("deu merda amigos\n");
			pthread_join(t[(i*m2 + j)%qtd], NULL);
		}
	}
	for(int i=0; i<n1; ++i){
		for(int j=0; j<m2; ++j){
			if(j) printf(" ");
			printf("%d", resultado[i][j]);
		}
		printf("\n");
	}
}
