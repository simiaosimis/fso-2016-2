Programa para demonstrar sinalização entre processos.
Desenvolvido no sistema operacional Debian 8, no compilador gcc versão 4.9.2. 
Código desenvolvido no editor de texto Vim.

Compilação:

	Para compilar o programa é necessário a execução do comando:
		Versão A (sequencial):
		$ make A
		Versão B (concorrente sem limite de thread)
		$ make B
		Versão C (concorrente com limite de thread)
		$ make C

	e agora existirá um binário com o nome de `prog`

Execução
	
	Para executar o programa, é necessário a execução do comando:
		$ ./prog
	Agora você deve informar as dimensões das 2 matrizes n1 (número 
	de linhas), m1 (numero de colunas), n2, m2;
	A seguir deve ser informado os valores da primeira matriz linha
	a linha, em sequencia igualmente deve ser feito para a segunda
	matriz;

Limitações
	Note que para a multiplicação das matrizes serem validas o número
	de colunas da primeira matriz deve ser igual ao numero de linhas
	da segunda matriz.
	Em questão de desempenho a versão mais lenta é a C, seguido da B,
	tendo a versão A como a mais eficiente
	Na versão A o tempo consumido é na execução dos calculos;
	Na versão B o tempo consumido é na maior parte do sistema;
	Na versão C é um misto dos dois.
