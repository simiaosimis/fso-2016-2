

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv){
  
  double number1 = 7.3, number2;
    char s1[100], s2[100];
  double *dPtr;
  dPtr = &number1;

  printf("Valor da variavel apontada por dPtr: %.1f\n", *dPtr);

  number2 = *dPtr;

  printf("Valor armazenado em number2: %.1f\n", number2);

  printf("Endereco de number1: %p\n", &number1);
  printf("Endereco armazenado em dPtr: %p\n", dPtr);

  printf("Escreva uma string de no maximo 50 caracteres: ");

    scanf ( "%[^\n]", s1);  

  strcpy(s2, s1);

  printf("String s1: [%s]\n", s1);
  printf("String s2: [%s]\n", s2);

  if(strcmp(s1,s2)==0){
    printf("As strings sao iguais\n");
  }
  else{
    printf("As strings sao diferentes\n");
  }

  strcat(s1, s2);

  printf("Strings s1 e s2 concatenadas: %s\n", s1);

  int length = strlen(s1);

  printf("Tamanho s1 + s2: %d\n", length);
  return 0;
}