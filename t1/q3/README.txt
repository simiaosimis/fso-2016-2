Programa para demonstração de operações com ponteiros e strings.
Desenvolvido no sistema operacional Debian 8, no compilador gcc versão 4.9.2. 
Código desenvolvido no editor de texto Vim.

Compilação:

	Para compilar o programa é necessário a execução do comando:
		$ make
	e agora existirá um binário com o nome de `prog`

Execução
	
	Para executar o programa, é necessário a execução do comando:
		$ ./prog
	Assim que o programa realizar as operações com os ponteiros, o programa irá pedir para que o usuário entre com uma string. A string deve conter até no máximo 50 caracteres, caso contrário, causa um erro na execução do programa.
		$ <string>.
	Então são realizadas operações na string enviada.
