#ifndef TRIANGULO_OPERACOES_H
#define TRIANGULO_OPERACOES_H

#include "ponto.h"
#include "triangulo.h"

double trianguloValido(struct Triangulo t);
double perimetro(struct Triangulo t);
double distancia(struct Ponto p1, struct Ponto p2);
double area(struct Triangulo t);

#endif
