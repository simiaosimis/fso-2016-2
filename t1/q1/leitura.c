#include "leitura.h"
#include <stdio.h>

void lerTriangulo(struct Triangulo *t){
	scanf("%lf %lf %lf %lf %lf %lf", &t->p1.x, &t->p1.y
			, &t->p2.x, &t->p2.y, &t->p3.x, &t->p3.y);
}
