#include "triangulo.h"
#include "triangulo-operacoes.h"
#include "leitura.h"
#include <stdio.h>
#include <math.h>

int main(){
	struct Triangulo t;
	lerTriangulo(&t);
	if(fabs(trianguloValido(t)) < 1e-6){
		printf("O triangulo precisa ser valido\n");
		return 0;
	}
	printf("Lados: %lf %lf %lf\n", distancia(t.p1, t.p2),
			distancia(t.p1, t.p3), distancia(t.p2, t.p3));
	printf("Perimetro %lf\n", perimetro(t));
	printf("Area %lf\n", area(t));
}
