Programa para calculos de área, perímetro e lados de um dado triangulo.
Desenvolvido no sistema operacional Debian 8, no compilador gcc versão 4.9.2. 
Código desenvolvido no editor de texto Vim.

Compilação:

	Para compilar o programa é necessário a execução do comando:
		$ make
	e agora existirá um binário com o nome de `prog`

Execução:

	Para executar o programa, é necessário a execução do comando:
		$ ./prog
	Agora você deve entrar com os 3 pontos no plano 2D do triangulo
		$ x0 y0 x1 y1 x2 y2

	OBS: Um triangulo é considerado válido se os 3 pontos não são colineares, ou seja, possuem área positiva. Em caso de um triangulo inválido o programa será abortado, caso contrário serão apresentados os lados, área e perímetro do triangulo.
