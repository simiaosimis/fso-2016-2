#include "triangulo-operacoes.h"
#include <math.h>

double trianguloValido(struct Triangulo t){
	return (t.p1.x - t.p3.x)*(t.p2.y - t.p3.y) - (t.p1.y - t.p3.y)*(t.p2.x - t.p3.x);
}

double perimetro(struct Triangulo t){
	double l1 = distancia(t.p1, t.p2);
	double l2 = distancia(t.p1, t.p3);
	double l3 = distancia(t.p3, t.p2);
	return l1+l2+l3;
}

double area(struct Triangulo t){
	double l1 = distancia(t.p1, t.p2);
	double l2 = distancia(t.p1, t.p3);
	double l3 = distancia(t.p3, t.p2);
	double sm = (l1+l2+l3)/2.0;
	return sqrt(sm*(sm-l1)*(sm-l2)*(sm-l3));
}

double distancia(struct Ponto p1, struct Ponto p2){
	return hypot(p1.x - p2.x, p1.y - p2.y);
}
