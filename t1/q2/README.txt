Programa para ordenação de uma lista de inteiros.
Desenvolvido no sistema operacional Debian 8, no compilador gcc versão 4.9.2. 
Código desenvolvido no editor de texto Vim.

Compilação:

	Para compilar o programa é necessário a execução do comando:
		$ make
	e agora existirá um binário com o nome de `prog`

Execução:
	
	Para executar o programa, é necessário a execução do comando:
		$ ./prog <tipo de ordenação> <valores a serem ordenados>
		onde :
			<tipo de ordenação> pode ser -d para ordenado de menor para maior
									   e -r para ordenado de maior para menor
			<valores a serem ordenador> são números inteiros entre -2147483647 e 2147483647
