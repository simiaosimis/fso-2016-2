#include <string.h>
#include "quick_sort.h"

void quick_sort(int *array, int left, int right, char *flag){
	int pivot = left, i, ch, j;
	for (i = left; i <= right; i++)
	{
		j = i;
		if (strcmp(flag, "-d") == 0){
			if(array[j] < array[pivot]){
				ch = array[j];
				while(j > pivot){
					array[j] = array[j-1];
					j--;
				}
				array[j] = ch;
				pivot++;
			}
		}
		else{
			if(array[j] > array[pivot]){
				ch = array[j];
				while(j > pivot){
					array[j] = array[j-1];
					j--;
				}
				array[j] = ch;
				pivot++;
			}
		}
	}

	if(pivot-1 >= left){
		quick_sort(array, left, pivot-1, flag);
	}

	if(pivot+1 <= right){
		quick_sort(array, pivot+1, right, flag);
	}
}
