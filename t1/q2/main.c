#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "quick_sort.h"
#include "print.h"

int main(int argc, char* argv[]){

	if(argc <= 1){
		printf("O programa nao pode ser executado dessa forma. \nSao necessarios uma flag e um ou mais numeros.\n");
		return 0;
	}

	if(argc <= 2){
		printf("O programa nao pode ser executado dessa forma. \nSao necessarios um ou mais numeros.\n");
		return 0;
	}

	int size = argc-2, i;

	int *numbers = (int*) malloc (size*sizeof(int));
	char *flag = strdup(argv[1]);

	if(strcmp(flag, "-d") != 0 && strcmp(flag, "-r") != 0){
		printf("Flag nao pode ser diferente de -d ou -r\n");
		return 0;
	}

	for(i = 2; i < argc; i++){
		numbers[i-2] = atoi(argv[i]);
	}

	printf("Os numeros sao:\n");

	print_numbers(numbers, size);

	printf("\n");

	quick_sort(numbers, 0, size-1, flag);

	printf("Os numeros ordenados sao:\n");
	
	print_numbers(numbers, size);

	printf("\n");

	return 0;
}