Programa para demonstrar sincronização de processos e threads.
Desenvolvido no sistema operacional Debian 8, no compilador gcc versão 4.9.2. 
Código desenvolvido no editor de texto Vim.

Compilação:

	Para compilar o programa é necessário a execução do comando:
		$ make
	e agora existirá um binário com o nome de 'prog'


Execução
	
	Para executar o programa, é necessário a execução do comando:
		$ ./prog
	Logo após, é necessário entrar com um número de 1 a 10. Caso seja inserido um número maior que 10, o programa diminui o número inserido para 10.
	Após inserir o número, o programa irá disparar irá disparar x threads, sendo x o número inserido. Cada thread receberá como parâmetro o seu número (1 a 10). O trabalho da thread 1 será imprimir uma linha com um caractere a enquanto o trabalho da thread 2 será imprimir uma linha com dois caracteres b e assim sucessivamente. As thread tem um intervalo de 500ms entre impressões.
	O programa ira esperar o sinal SIGINT (Ctrl+C) para finalizar as threads e ao final, mostra um relatório das threads.

	Um exemplo de tela seria:
	$ ./prog
	7
	a
	ffffff
	ggggggg
	dddd
	eeeee
	bb
	ccc
	ffffff
	bb
	dddd
	a
	ggggggg
	eeeee
	ccc
	a
	ccc
	ffffff
	dddd
	bb
	eeeee
	ggggggg
	dddd
	eeeee
	ccc
	ffffff
	bb
	ggggggg
	a
	dddd
	eeeee
	ccc
	ffffff
	bb
	a
	ggggggg
	^CEncerrando a aplicação. Aguardando finalização de threads....
	Aplicação encerrada com sucesso!
	Estatísticas:
	thread 1: 5 linhas
	thread 2: 5 linhas
	thread 3: 5 linhas
	thread 4: 5 linhas
	thread 5: 5 linhas
	thread 6: 5 linhas
	thread 7: 5 linhas


Programa

	O programa instancia N threads onde cada uma delas roda em um loop com um useelp de 500000 microsegundos, e se aproveita do fato de a função printf do stdio ser thread safe para imprimir cada linha pré-processada de caracteres.
	Quando acionado o sinal de SIGINT (ctrl c) o programa gera o relatório através de um contador global para cada thread. E finaliza cada uma das N threads.
